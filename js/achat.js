function fullscreen() { //Permet de faire un zoom sur l'image
    var modal = document.getElementById("modalIMG");
    var img = document.getElementById("modalContent");
    var imgSource = document.getElementById("sourceIMG");
    var close = document.getElementsByClassName("close")[0];

    //On affecte l'image source a l'image modal
    img.src = imgSource.src;
    img.alt = imgSource.alt;

    //Si canape on affiche le tableau
    if(imgSource.className == "canapeImg") document.getElementById("modalHover").style.display = "block";
        else document.getElementById("modalHover").style.display = "none";

    modal.style.display = "block";

    close.onclick = function() {
        modal.style.display = "none";
    }
}

function changePhoto(img) { //Change la photo principale de l'aperçu
    var imgSource = document.getElementById("sourceIMG");
    imgSource.src = img.src;
    imgSource.alt = img.alt;
    imgSource.className = img.id;
    if(img.id == "canapeImg") document.getElementById("sourceHover").style.display = "block";
        else document.getElementById("sourceHover").style.display = "none";
}

function changeTab(tab) {
    //Cache le contenu des tabs
    for(i = 0; i < document.getElementsByClassName("tabContent").length; i++) {
        document.getElementsByClassName("tabContent")[i].style.display = "none";
        document.getElementsByClassName("btnChoose")[i].style.display = "none";//Désactive le bouton lié (peut le faire car une tab a forcement un bouton)
    }

    //Retire le nom de classe active
    if(document.getElementsByClassName("active").length > 0) document.getElementsByClassName("active")[0].className = "tabContent";

    //Affiche la tab voulue
    document.getElementById(tab).style.display = "block";
    document.getElementById(tab).className += " active";

    //Affiche le bouton associer
    if(document.getElementById(tab + "Btn") != null) {
        document.getElementById(tab + "Btn").style.display = "block";
    }

    //Si la tab est cadre on affiche le bouton ajouter au panier
    if(tab == "cadre") document.getElementById('panierBtn').style.display = "block";

    //Desactive le button précédent
    if(tab === "format") document.getElementById("prevBtn").disabled = true;
        else document.getElementById("prevBtn").disabled = false;
}

function changeTabLink(movement, tab) {

    var tbLink = document.getElementsByClassName("tabLink");

    //Parcours le tableau tbLink pour trouver la tab correspondante
    for(i = 0; i < tbLink.length; i++) {
        if (tbLink[i].name == tab) {
            if (movement) {
                changeTab(tbLink[i+1].name);
                //Rend le bouton suivant disabled a nouveau
                document.getElementById('nextBtn').disabled = true;
            } else {
                //On déselectionne les éléments
                if(tab == 'finition') {
                    if(document.getElementsByClassName("chosenFinition").length > 0) {
                        document.getElementsByClassName("chosenFinition")[0].style.border = "";
                        document.getElementsByClassName("chosenFinition")[0].className = "finForm";
                        document.getElementById('finitionBtn').disabled = true;
                        document.getElementById('cadreTab').disabled = true;
                    } 
                } else {
                    if(document.getElementsByClassName("chosenCadre").length > 0) {
                        document.getElementsByClassName("chosenCadre")[0].style.border = "";
                        document.getElementsByClassName("chosenCadre")[0].className = "cadreForm";
                    } 
                }
                //On change de tab
                changeTab(tbLink[i-1].name);
                //Retour en arrière on rend le bouton suivant cliquable a nouveau
                document.getElementById('nextBtn').disabled = false;
            }
        }
    }

}

function formatForm(format) {
    format = document.getElementById(format);
    if(document.getElementsByClassName("chosenFormat")[0] != format) {
        //Désactive le cadre tab au changement, si on reviens en arrière avec les tabs, et que l'on change de format
        document.getElementById('cadreTab').disabled = true;
        //Retire la class chosenFinition
        if(document.getElementsByClassName("chosenFinition").length > 0) {
            document.getElementsByClassName('chosenFinition')[0].style.border = "";
            document.getElementsByClassName("chosenFinition")[0].className = "finForm";
        } 
        //Applique la bordure a l'élement cliquer
        format.style.border = "1px solid #F6546A";
        //Retire la classe chossenFormat
        if(document.getElementsByClassName("chosenFormat").length > 0){
            document.getElementsByClassName("chosenFormat")[0].style.border = "";
            document.getElementsByClassName("chosenFormat")[0].className = "formDiv";
        } 
        //Appplique la classe chossenFormat
        format.className += " chosenFormat";
        //Rend les boutons cliquable
        document.getElementById('formatBtn').disabled = false;
        document.getElementById('nextBtn').disabled = false;
        document.getElementById('finitionTab').disabled = false;
        //Met a jour le prix
        updatePrice(format.getElementsByTagName('prixFormat')[0].innerHTML, 'format');

        //Affiche le bloc voulu dans la tab finition
        if(format.id == 'grand' || format.id == 'geant' || format.id == 'collector') {
            document.getElementById('otherFormat').style.display = 'block';
            document.getElementById('classiqueFormat').style.display = 'none';
        } else {
            document.getElementById('classiqueFormat').style.display = 'block';
            document.getElementById('otherFormat').style.display = 'none';
        }

        changeSize(format)
    }
}

function finitionForm(finition) {
    finition = document.getElementById(finition);
    if(document.getElementsByClassName("chosenFinition")[0] != format) {
        //Applique la bordure a l'élement cliquer
        finition.style.border = "1px solid #F6546A";
        //Si le format choisi est classique on ne va pas dans cadre
        if (finition.id == 'papier') {
            document.getElementById('nextBtn').disabled = true;
            document.getElementById('cadreTab').disabled = true;
            document.getElementById('finitionBtn').style.display = "none";
            document.getElementById('panierBtn').style.display = "block";
            document.getElementById('panierBtn').disabled = false;
        } else {
            document.getElementById('finitionBtn').disabled = false;
            document.getElementById('nextBtn').disabled = false;
            document.getElementById('cadreTab').disabled = false;
            document.getElementById('finitionBtn').style.display = "block";
            document.getElementById('panierBtn').style.display = "none";
            document.getElementById('panierBtn').disabled = true;
        }
        //Retire la class chosenFinition
        if(document.getElementsByClassName("chosenFinition").length > 0) {
            document.getElementsByClassName('chosenFinition')[0].style.border = "";
            document.getElementsByClassName("chosenFinition")[0].className = "finForm";
        } 
        //Affiche le bloc voulu dans la tab cadre
        if(finition.id == "blackout" || finition.id == "artshot") {
            document.getElementById("supportAlu").style.display = "none";
            document.getElementById("passePartout").style.display = "block";
        } else {
            document.getElementById("supportAlu").style.display = "block";
            document.getElementById("passePartout").style.display = "none";
        }
        //Appplique la classe chossenFormat
        finition.className += " chosenFinition";
        //Met a jour le prix
        updatePrice(finition.getAttribute('data-price'), 'finition');
    }
}


function cadreForm(cadre) {
    cadre = document.getElementById(cadre);
    if(document.getElementsByClassName("chosenCadre")[0] != cadre) {
        //Applique la bordure a l'élement cliquer
        cadre.style.border = "1px solid #F6546A";
        //Retire la class chosenFinition
        if(document.getElementsByClassName("chosenCadre").length > 0) {
            document.getElementsByClassName('chosenCadre')[0].style.border = "";
            document.getElementsByClassName("chosenCadre")[0].className = "cdrForm";
        } 
        //Appplique la classe chossenFormat
        cadre.className += " chosenCadre";
        //Met a jour le prix
        updatePrice(cadre.getAttribute('data-price'), 'cadre');
    }
}

function updatePrice(price, type) {
    switch(type) {
        case 'format':
            document.getElementById('prix').innerHTML = price;
            break;
        case 'finition':
            var formatPrice = parseFloat(document.getElementsByClassName("chosenFormat")[0].getElementsByTagName('prixFormat')[0].innerHTML);
            document.getElementById('prix').innerHTML = (formatPrice + (formatPrice * price));
            break;
        case 'cadre':
            var formatPrice = parseFloat(document.getElementsByClassName("chosenFormat")[0].getElementsByTagName('prixFormat')[0].innerHTML);
            var finitionPrice = document.getElementsByClassName("chosenFinition")[0].getAttribute('data-price')[0];
            var formatFinitionPrice = formatPrice + (formatPrice * finitionPrice);
            document.getElementById('prix').innerHTML = (formatFinitionPrice + (formatFinitionPrice * price)).toFixed(2);
            break;
    }
}

function definePrice(price) {
    var classiquePrice = document.getElementById("classique").getElementsByTagName('prixFormat')[0];
    var grandPrice = document.getElementById("grand").getElementsByTagName('prixFormat')[0];
    var geantPrice = document.getElementById("geant").getElementsByTagName('prixFormat')[0];
    var collectorPrice = document.getElementById("collector").getElementsByTagName('prixFormat')[0];

    classiquePrice.innerHTML = price + (price * 0.3);
    grandPrice.innerHTML = classiquePrice.innerHTML * 2;
    geantPrice.innerHTML = grandPrice.innerHTML * 2;
    collectorPrice.innerHTML = geantPrice.innerHTML * 2;
}

function changeSize(format) {
    var sourceImg = document.getElementById("sourceHover"); //Lorsque l'image n'est pas en fullscreen
    var modalImg = document.getElementById("modalHover"); //Lorsque l'image est en fullscreen
    switch(format.id) {
        case("grand"):
            sourceImg.style.width = "25%";
            sourceImg.style.top = "10%";
            sourceImg.style.left = "50%";
            //modal
            modalImg.style.width = "15%";
            modalImg.style.top = "21%";
            modalImg.style.left = "52%";
            break;
        case("geant"): //2ème format plus grand format
            sourceImg.style.width = "30%";
            sourceImg.style.top = "8%";
            sourceImg.style.left = "48%";
            //modal
            modalImg.style.width = "20%";
            modalImg.style.top = "17%";
            modalImg.style.left = "48%";
            break;
        case("collector"): //Le plus grand format
            sourceImg.style.width = "35%";
            sourceImg.style.top = "3%";
            sourceImg.style.left = "48%";
            //modal
            modalImg.style.width = "25%";
            modalImg.style.top = "12%";
            modalImg.style.left = "47%";
            break;
        case("classique"): //Le plus petit format
            sourceImg.style.width = "20%";
            sourceImg.style.top = "10%";
            sourceImg.style.left = "53%";
            //modal
            modalImg.style.width = "10%";
            modalImg.style.top = "24%";
            modalImg.style.left = "55%";
            break;
    }
}