checkboxIsCheckForWatchDelete("orientation_check", "clear_orientation");
checkboxIsCheckForWatchDelete("format_check", "clear_format");
checkboxIsCheckForWatchDelete("price_check", "clear_price");

function checkboxIsCheckForWatchDelete(listClassCheckbox, buttonDelete){
    let orientation_check = document.getElementsByClassName(listClassCheckbox);
    let count = 0;
    for (const checkBox of orientation_check) {
        checkBox.addEventListener('change', function(){
            count = checkBoxisChecked(listClassCheckbox);
            if (count > 0){
                document.getElementById(buttonDelete).style.display = "inline";
            }else{
                document.getElementById(buttonDelete).style.display = "none";
            }
        });
    }
}

function checkBoxisChecked(listClassCheckbox){
    let orientation_check = document.getElementsByClassName(listClassCheckbox);
    let count = 0;
    for (const checkBox of orientation_check) {
        if (checkBox.checked){
            count++;
        }
    }
    return count;
}

function unchecking(classCheckBox, buttonDelete){
    const checkBoxs = document.getElementsByClassName(classCheckBox);
    for (const checkBox of checkBoxs) {
        checkBox.checked = false;
        buttonDelete.style.display = "none";
    }
}